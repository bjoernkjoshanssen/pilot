# Introduction #

This README documents the steps that are necessary to compile the Report on a Pilot Assessment Exam, Spring 2015.

### What is this repository for? ###

* Use R and SWeave to compile the report
* Version 1

### How do I get set up? ###

* Install R
* Use the command Sweave("pilot") from within R
* Install LaTeX

### Who do I talk to? ###

* Bjørn Kjos-Hanssen